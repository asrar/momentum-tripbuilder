## TripBuilder

Trip Builder is a web service (API) that serves as the engine for front-end clients to manage trips for their customers.

## Installation

Local installation:

If you do not have Laravel installed, please follow the Installing Laravel instructions here: https://laravel.com/docs/5.3/installation

Once Laravel is installed locally, setup should be as simple as this:


```
$ git clone https://asrar@bitbucket.org/asrar/momentum-tripbuilder.git tripbuilder-asrar
$ cd tripbuilder-asrar
$ composer install
$ npm install
$ php artisan serve

```

## Testing

To test POST and DELETE requests, I recommend using the [Postman](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop?hl=en) extension for Google Chrome.

## API Description

### GET /airports

Get a list of all the airports.

### GET /airports/{iata}

Get a single airport by airport code (IATA).

### GET /trips

Get a list of all trips (two default records have been added to the database automatically).

### GET /trips/{trip_id}

Get a single trip.

### GET /trips/{trip_id}/flights

Get a single trip's flights.

### POST /trips/{trip_id}/flights

Add a flight to a trip with post data, for example: ['iata' => 'YUL'].

### DELETE /trips/{trip_id}/flights/{flight_id}

Deletes a flight from a trip.

## Credits

This application was developed by Asrar Abbasi for Momentum Travel. It was built using the Laravel 5 PHP framework.
