<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/airports', 'AirportsController@listing');
Route::get('/airports/{code}', 'AirportsController@single');

Route::get('/trips', 'TripsController@listing');
Route::get('/trips/{trip}', 'TripsController@single');
Route::get('/trips/{trip}/flights', 'TripsController@flights');
Route::post('/trips/{trip}/flights', 'TripsController@addFlight');
Route::delete('/trips/{trip}/flights/{flight}', 'TripsController@deleteFlight');
