<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trip;
use App\TripFlights;
use App\Airport;

class TripsController extends Controller
{
    /**
    * Return a listing of all trips.
    */
    public function listing()
    {
        return Trip::all();
    }

    /**
    * Get the details of a single trip.
    * @param int $trip
    */
    public function single(Trip $trip)
    {
        return $trip;
    }

    /**
    * Get all the flights for a single trip.
    * @param int $trip
    */
    public function flights(Trip $trip)
    {
        return $trip->flights()->get();
    }

    /**
    * Add a flight to a trip via POST request.
    * @param int $trip
    */
    public function addFlight(Trip $trip)
    {
        $data = request()->all();
        $errors = [];
        $response = null;
        $success = false;

        if (isset($data['iata'])) {
            $data['iata'] = strtoupper($data['iata']);

            if (empty(Airport::where('iata', $data['iata'])->first())) {
                $errors[] = 'The IATA '. $data['iata'] .' is not valid.';
            }

            $current_airport = $trip->flights()->orderBy('id', 'desc')->first();
            if (!empty($current_airport) && $current_airport->iata == $data['iata']) {
                $errors[] = 'You are already at '. $data['iata'] .'. Please select a different airport.';
            }
        } else {
            $errors[] = 'You must specify the IATA.';
        }

        if (empty($errors)) {
            $flight = new TripFlights;
            $flight->iata = $data['iata'];

            $response = $trip->flights()->save($flight);

            $success = (bool)$response;
        }

        return json_encode([
            'success' => $success,
            'response' => $response,
            'errors' => $errors
        ]);
    }

    /**
    * Delete flight from a trip via DELETE request.
    * @param int $trip
    * @param int $flight_id
    */
    public function deleteFlight(Trip $trip, $flight_id)
    {
        $success = false;
        $errors = [];

        $flight_belongs_to_trip = $trip->flights()->find($flight_id);
        if ($flight_belongs_to_trip) {
            $success = $flight_belongs_to_trip->delete();
        } else {
            $errors[] = 'The flight ID '. $flight_id .' does not belong to trip: '. $trip->name;
        }

        return json_encode([
            'success' => $success,
            'errors' => $errors
        ]);
    }
}
