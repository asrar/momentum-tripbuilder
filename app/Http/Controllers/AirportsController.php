<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Airport;

class AirportsController extends Controller
{
    
    /**
    * Returns a listing of all airport codes
    */
    public function listing()
    {
        return Airport::orderBy('iata')->get();
    }

    /**
    * Returns a single airport
    * @param string $iata airport code
    */
    public function single($iata)
    {
        return Airport::where('iata', strtoupper($iata))->first();
    }
}
