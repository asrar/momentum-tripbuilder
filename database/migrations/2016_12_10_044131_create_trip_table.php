<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('trip_flights', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trip_id');
            $table->string('iata');
            $table->timestamps();
        });

        // Add some data to trips table. In a normal application,
        // this data would be populated by users beginning their search.
        
        DB::table('trips')->insert([
            ['name' => 'Frodo\'s trip'],
            ['name' => 'C3PO\'s trip'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip');
    }
}
