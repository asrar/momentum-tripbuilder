<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Trip Builder</title>

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h1>Trip Builder</h1>
                    <hr />
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>Airports</th>
                                <td>GET <a href="/airports">/airports</a></td>
                            </tr>
                            <tr>
                                <th>Single Airport (YUL)</th>
                                <td>GET <a href="/airports/yul">/airports/yul</a></td>
                            </tr>
                            <tr>
                                <th>Trips</th>
                                <td>GET <a href="/trips">/trips</a></td>
                            </tr>
                            <tr>
                                <th>Frodo's Trip</th>
                                <td>GET <a href="/trips/1">/trips/1</a></td>
                            </tr>
                            <tr>
                                <th>Frodo's Trip Flights</th>
                                <td>GET <a href="/trips/1/flights">/trips/1/flights</a></td>
                            </tr>
                            <tr>
                                <th>C3PO's Trip</th>
                                <td>GET <a href="/trips/2">/trips/2</a></td>
                            </tr>
                            <tr>
                                <th>C3PO's Trip Flights</th>
                                <td>GET <a href="/trips/2/flights">/trips/2/flights</a></td>
                            </tr>
                            <tr>
                                <th>Add a flight</th>
                                <td>POST /trips/{trip_id}/flights</td>
                            </tr>
                            <tr>
                                <th>Delete a flight</th>
                                <td>DELETE /trips/{trip_id}/flights/{flight_id}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
