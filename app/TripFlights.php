<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripFlights extends Model
{
    public function trip()
    {
        return $this->belongsTo(App\Trip::class);
    }
}
