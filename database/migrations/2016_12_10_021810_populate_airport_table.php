<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateAirportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        $json_data = json_decode(file_get_contents(storage_path('airports.json')));
        foreach($json_data as $data) {
            if (isset($data->iata) 
                && isset($data->type) && $data->type == 'airport' 
                && isset($data->name) && !empty($data->name)) {
                $insert = [
                    'iata' => $data->iata,
                    'name' => isset($data->name) ? $data->name : "",
                    'country' => isset($data->iso) ? $data->iso : "",
                    'continent' => isset($data->continent) ? $data->continent : "",
                    'lat' => isset($data->lat) ? $data->lat : 0,
                    'lng' => isset($data->lon) ? $data->lon : 0,
                ];

                DB::table('airports')->insert($insert);
            }
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('airports')->truncate();
    }
}
